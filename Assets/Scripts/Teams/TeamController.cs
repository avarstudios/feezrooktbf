﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//[CreateAssetMenu(fileName = "New Team", menuName = "Team", order = 51)]
public class TeamController : MonoBehaviour
{

    [SerializeField]
    private string teamName;

    public string TeamName
    {
        get { return teamName; }
    }


    [SerializeField]
    private Color teamColor;

    public Color TeamColor
    {
        get { return teamColor; }
    }

    [SerializeField]
    private PlayerController[] players;

    public PlayerController[] Players
    {
        get { return players; }
    }

    [SerializeField]
    private PlayerController goalkeeper;

    public PlayerController NearestToBallPlayer
    {
        get
        {
            PlayerController nearestPlayer = null;
            float bestDistance = float.MaxValue;
            Vector3 ballPos = GameManager.Instance.Ball.transform.position;
            foreach (var testedPlayer in players)
            {
                Vector3 testedPlayerPos = testedPlayer.transform.position;
                float currDistance = Vector3.Distance(testedPlayerPos, ballPos);
                if (currDistance < bestDistance)
                {
                    nearestPlayer = testedPlayer;
                    bestDistance = currDistance;
                }
            }

            return nearestPlayer;
        }
    }

    public PlayerController Goalkeeper
    {
        get { return goalkeeper; }
    }

    public PlayerController GetNextPlayer(PlayerController currentPlayer)
    {
        print("Select next player after " + currentPlayer.name);

        if (currentPlayer == goalkeeper)
            return players[0];

        int playerIndex = GetPlayerByIndex(currentPlayer);
        playerIndex++;
        if (players.Length <= playerIndex)
            if (goalkeeper != null)
                return goalkeeper;
            else
                playerIndex = 0;

        return players[playerIndex];
    }

    public PlayerController GetPreviousPlayer(PlayerController currentPlayer)
    {
        print("Select previous player after " + currentPlayer.name);

        if (currentPlayer == goalkeeper)
            return players[players.Length-1];

        int playerIndex = GetPlayerByIndex(currentPlayer);
        playerIndex--;
        if (playerIndex < 0)
            if (goalkeeper != null)
                return goalkeeper;
            else
                playerIndex = players.Length-1;

        return players[playerIndex];

    }

    int GetPlayerByIndex(PlayerController currentPlayer)
    {
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i] == currentPlayer)
                return i;
        }

        return 0;
    }

    public bool IsControllBall()
    {
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].IsControllBall)
                return true;
        }
        return false;
    }

    public List<PlayerController> GetPlayersInArea(Vector3 border1, Vector3 border2)
    {
        List<PlayerController> result = new List<PlayerController>();

        foreach (var currentPlayer in players)
        {
            if (GameMath.IsPointInZone(currentPlayer.transform.position, border1, border2))
                result.Add(currentPlayer);
        }

        return result;
    }

}
