﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraController : MonoBehaviour {
    [SerializeField]
    private float speed; // скорость перемещения камеры

    bool isPaused;
    BallController ball;

    void Start()
    {
        GameManager.OnCommandPhaseStarted += Pause;
        GameManager.OnPlayPhaseStarted += Resume;
        ball = GameManager.Instance.Ball;
    }

    // Update is called once per frame
    void Update () {

        if (isPaused)
            return;

        // во время игры следим за тем, где мяч
        Vector3 targetPos = GetStearingPos();
        float distance = Vector3.Distance(new Vector3(0, 0, targetPos.z), new Vector3(0, 0, transform.position.z));
        if (distance > 1)
        {
            Vector3 newPos = new Vector3(transform.position.x, transform.position.y, targetPos.z);
            transform.position = Vector3.MoveTowards(transform.position, newPos, distance / speed);
        }
    }

    public void Pause()
    {
        isPaused = true;
    }

    public void Resume()
    {
        isPaused = false;
    }

    Vector3 GetStearingPos()
    {
        if (GameManager.Instance.Team1.IsControllBall())
            return GameManager.Instance.Team1.NearestToBallPlayer.transform.position;
        else if (GameManager.Instance.Team2.IsControllBall())
            return GameManager.Instance.Team2.NearestToBallPlayer.transform.position;
        else
            return ball.transform.position;

    }
}
