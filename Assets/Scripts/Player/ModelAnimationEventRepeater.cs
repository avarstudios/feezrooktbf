﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelAnimationEventRepeater : MonoBehaviour {
    public event System.Action OnKickBallInAnimation;
    public event System.Action OnStandingUpInAnimation;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void KickBall()
    {
        print(name + " Animation event KickBall");

        if (OnKickBallInAnimation != null)
            OnKickBallInAnimation();
    }

    public void StartStandingUp()
    {
        print(name + " Animation event StartStandingUp");

        if (OnStandingUpInAnimation != null)
            OnStandingUpInAnimation();
    }
}
