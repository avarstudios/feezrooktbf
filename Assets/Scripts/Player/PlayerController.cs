﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour {

    public static event System.Action<PlayerController> OnMouseSelected;
    public event System.Action OnTakeBall;
    public event System.Action OnLooseBall;

    private Animator animator;
    private PlayerActionController actionController;

    [SerializeField]
    private GameObject selectedIndicator;
    Vector3 selectedIndicatorBaseScale;

    [SerializeField]
    GameObject arrow;

    ActionArrowController arrowController;

    [SerializeField]
    public PlayerAction Action1;

    [SerializeField]
    public PlayerAction Action2;

    bool firstActionUpdated;
    bool isSelected;
    bool onPause;

    NavMeshAgent navAgent;

    Vector3 targetPos;

    [SerializeField]
    private float ballControllRange;

    public float BallControllRange
    {
        get
        {
            return ballControllRange;
        }
    }

    bool canTakeBall;

    [SerializeField]
    private float ballReactionRange = 25.0f;
    public float BallReactionRange
    {
        get
        {
            return ballReactionRange;
        }
    }

    [SerializeField]
    private bool isControllBall;

    public bool IsControllBall
    {
        get
        {
            return isControllBall;
        }
    }

    public Vector3 TargetPos
    {
        get { return targetPos; }
    }

    private TeamController team;
    public TeamController Team
    {
        get { return team; }
    }

    private BallController ball;

    public string Number { get; set; }
    public string Name { get; set; }

    void Start () {
        animator = GetComponentInChildren<Animator>();
        navAgent = GetComponent<NavMeshAgent>();
        actionController = GetComponent<PlayerActionController>();
        actionController.OnActionEnded += OnActionEnded;
        actionController.OnBallKicked += BallKicked;
        team = GetComponentInParent<TeamController>();
        ball = GameManager.Instance.Ball;

        GameManager.OnCommandPhaseStarted += Pause;
        GameManager.OnPlayPhaseStarted += Resume;

        canTakeBall = true;
        selectedIndicatorBaseScale = selectedIndicator.transform.localScale;

        Name = name;
        Number = "05";
        Number = UnityEngine.Random.Range(10, 99).ToString();

        Unselect();
     }
	
	void Update () {
		
        if(isSelected)
        {
            float scale = 1 + Mathf.Sin(Time.timeSinceLevelLoad * 2.5f) * 0.25f;
            selectedIndicator.transform.localScale = new Vector3(scale, scale, scale);
        }

        if (Vector3.Distance(transform.position, ball.transform.position) <= ballControllRange)
            TryToTakeBall();
	}

    public void Pause()
    {
        actionController.Pause();
        ShowActionsArrows();
    }

    public void Resume()
    {
        HideActionsArrows();

        actionController.Resume();
        if (firstActionUpdated)
        {
            actionController.StartNewAction(Action1);
            firstActionUpdated = false;
        }
    }

    private void OnMouseDown()
    {
        OnPlayerSelected();
    }

    public void OnPlayerSelected()
    {
        if (OnMouseSelected != null)
            OnMouseSelected(this);
    }

    public void Select()
    {
        isSelected = true;

        selectedIndicator.SetActive(true);
    }

    public void Unselect()
    {
        isSelected = false;
        selectedIndicator.SetActive(false);
        selectedIndicator.transform.localScale = selectedIndicatorBaseScale;
        //firstActionUpdated = false;
    }

    public void SetTargetMovePosition(Vector3 newPos)
    {
        targetPos = newPos;
        Action1 = GetNewAction(EPlayerAction.Move, transform.position, newPos);
        Action2 = null;

        HideActionsArrows();
        ShowActionsArrows();

        firstActionUpdated = true;
    }

    public void SetAction(EPlayerAction action, Vector3 actionTargetPos, Vector3 border1, Vector3 border2, PlayerController anotherPlayer = null)
    {
        if(firstActionUpdated)
        {
            Action2 = GetNewAction(action, Action1.TargetPosition, actionTargetPos);
            Action2.ActionZoneBorder1 = border1;
            Action2.ActionZoneBorder2 = border2;
        }
        else
        {
            Action1 = GetNewAction(action, transform.position, actionTargetPos);
            Action1.ActionZoneBorder1 = border1;
            Action1.ActionZoneBorder2 = border2;

            Action2 = null;
            firstActionUpdated = true;
        }

        HideActionsArrows();
        ShowActionsArrows();
    }

    public void SetAction(EPlayerAction action)
    {
        if(action == EPlayerAction.Tackle || action == EPlayerAction.TackleAnyCost)
        {
            Vector3 actionTargetPos = ball.transform.position;
            SetAction(action, actionTargetPos, actionTargetPos, actionTargetPos);
        }
        else if (action == EPlayerAction.TakePass)
        {
            SetAction(action, transform.position, Vector3.zero, Vector3.zero);
        }

    }

    void OnActionEnded()
    {
        Action1 = null;
        if (Action2 != null)
        {
            Action1 = Action2;
            Action2 = null;
            actionController.StartNewAction(Action1);
        }
        else
        {
        }
    }

    void ShowActionsArrows()
    {
        if (team != GameManager.Instance.CurrentCoatchTeam)
            return;

        Vector3 shift = new Vector3(0, 0.2f, 0);

        if (Action1 != null)
        {
            if(arrowController == null)
            {
                GameObject newArrow = Instantiate<GameObject>(arrow);
                newArrow.transform.position = transform.position;
                newArrow.transform.SetParent(transform);
                arrowController = newArrow.GetComponent<ActionArrowController>();
            }
            arrowController.Show(Action1.Action, transform.position + shift, Action1.TargetPosition + shift);
        }

        if (Action2 != null)
        {
            if (arrowController == null)
            {
                GameObject newArrow = Instantiate<GameObject>(arrow);
                newArrow.transform.position = transform.position;
                newArrow.transform.SetParent(transform);
                arrowController = newArrow.GetComponent<ActionArrowController>();
            }
            arrowController.Show(Action2.Action, Action2.StartPosition + shift, Action2.TargetPosition + shift);
        }
    }

    void HideActionsArrows()
    {
        if (arrowController != null)
        {
            arrowController.Hide();
            arrowController = null;
        }
    }

     PlayerAction GetNewAction(EPlayerAction action, Vector3 startPos, Vector3 endPos)
    {
        PlayerAction newAction = new PlayerAction();
        newAction.Action = action;
        newAction.StartPosition = startPos;
        newAction.TargetPosition = endPos;

        return newAction;
    }

    public bool IsBallInReactionRange()
    {
        return Vector3.Distance(transform.position, ball.transform.position) <= ballReactionRange;
    }

    public bool IsBallInReactionRange(Vector3 position)
    {
        return Vector3.Distance(position, ball.transform.position) <= ballReactionRange;
    }

    public bool IsBallInReactionRange(bool includeFuturePositions = false)
    {
        if (!includeFuturePositions)
            return IsBallInReactionRange();

        int pointsCount = 1;
        if (Action1 != null)
            pointsCount++;

        Vector3[] positions = new Vector3[pointsCount];
        positions[0] = transform.position;
        if (Action1 != null)
            positions[1] = Action1.TargetPosition;

        for (int i = 0; i < pointsCount; i++)
        {
            if (IsBallInReactionRange(positions[i]))
                return true;
        }
        return false;
    }

    public void LooseBall(bool isVoluntary = false)
    {
        if (!isControllBall)
            return;

        isControllBall = false;
        canTakeBall = false;

        //ballImpulceCurrDistance = 0;
        if (OnLooseBall != null)
            OnLooseBall();

    }

    IEnumerator ResetBlockBallTaking()
    {
        yield return new WaitForSeconds(1);
        canTakeBall = true;
    }

    public void TakeBall()
    {
        if (isControllBall)
            return;

        isControllBall = true;
        if (OnTakeBall != null)
            OnTakeBall();
    }

    void BallKicked()
    {
        LooseBall(true);
    }

    void TryToTakeBall()
    {
        if (!canTakeBall || isControllBall || team.IsControllBall())
            return;

        isControllBall = true;
    }
}
