﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats
{
    // --------------------- Характеристики игрока
    /**
     *Уникальный номер игрока
     * (до записи в БД равен 0 )
     */
    public int id;

    /**
     *  Номер команды
     */

    public int teamId;
    /**
     * LastName or Suname (фамилия)
     */

    public string lastName;
    /**
     * Player name
     */

    public string firstName;
    
    /**
     * Age player (возраст игрока)
     */
    public int age;
    /**
     * Количество дне оставшихся до дня рождения
     * range 0..52
     */
    public int birthday;
    
    /**
     * Позиция игрока на поле
     */
    public int position;

    /**
     * Тип игрока 
    const GOALKEEP = 0; // Голкиперы
    const DEFENDER = 1; //Защитники DEFENDER
    const MIDFIELDER = 2; // Полузащитники  MIDFIELDER
    const ATTACKER = 3; // Нападающие ATTACKER
    const DEF_MID = 4; //Защитник полузащитник
    const MID_ATT = 5; // полузащитник атакующий
     */
    public int style;
    /**
     * Размещение игрока на фланге поля 
    Фланги 
    const FL_NONE = 0;
    const FL_LEFT = 1; //Л
    const FL_CENTER = 2; //Ц
    const FL_RIGHT = 3; // П
    const FL_LEFT_CENTER = 4; // ЛП
    const FL_LEFT_CENTER_RIGHT = 5; // ЛЦП
    const FL_CENTER_RIGHT = 6; // ЦП
    const FL_LEFT_RIGHT = 7; //ЛП
     */
    public int flang;

    /**
     * Number in team (номер игрока в команде)
     */
    public int number;
    
    /**
     * Player speed (cкорость)
     */
    public int pace;

    /**
     * Strength of player (сила игрока)
     */
    public int strength;
    
    /**
     * Stamina of player (выносливость игрока)
     */
    public int stamina;

    /**
     * (отбор)
     */
    public int takle;

    /**
     * (голова)
     */
    public  int heading;

    /**
     * (техника)
     */
    public int technic;

    /**
     * (пас)
     */
    public int pass;
    /**
     * Kick (удар)
     */
    public int kick;

    /**
     * Morale (мораль)
     */
    public int morale;

    /**
     * Fit (здоровье игрока)
     */
    public int fit;

    /**
     * Характер игрока см. константы класса (см. const CHERAHTER)
     * const CHERAHTERS = [1 => "бесконфликтный", 2 => "уравновешенный", 3 => "высокомерный", 4 => "вспыльчивый"];
     */
    public int character;

    /**
     * Опыт игрока
     */
    public int experience;

    /**
     * Стоимость игрока
     */
    public int price;

    /**
     * Зарплата игрока за матч (неделю)
     */
    public int wage;

    /**
     * @var
     */
    public int hairStyleType;
    /**
     * @var
     */
    public int hairStyleColor;
    /**
     * Skin
     */
    public int skin;
    /**
     * Тип обуви игрока
     * @var int $shoes
     */
    public int shoes;
    /**
     * Популярность
     * @var int
     */
    public int popularity;		
}
