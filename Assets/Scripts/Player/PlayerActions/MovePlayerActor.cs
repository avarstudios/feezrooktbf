﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayerActor : PlayerActor
{

    public override void Start ()
    {
        base.Start();
	}
	
	public override void Update ()
    {
        if (onPause || !isPlaying)
            return;

        base.Update();

        if (!onPause)
        {
            animator.SetFloat("Speed", navAgent.velocity.magnitude);
        }

        //print(Vector3.Distance(transform.position, targetPosition));

        if (targetPosition != Vector3.zero && Vector3.Distance(transform.position, targetPosition) < stoppingDistance)
        {
            print(name + ": Move action ended ");
            End();
        }
    }

    public override void Init()
    {
        
    }

    public override void Play()
    {
        print(name + ": Move action started ");
        isPlaying = true;
        navAgent.enabled = true;
        navAgent.destination = targetPosition;
        navAgent.speed = Speed;
        state = EActorState.MakeAction;  
    }

    public override void Stop()
    {
        //if (!isPlaying)
        //    return;

        print(name + ": Move action stoped");
        isPlaying = false;
        navAgent.enabled = false;
        animator.SetFloat("Speed", 0);
    }
}
