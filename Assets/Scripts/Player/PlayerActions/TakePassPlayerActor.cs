﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakePassPlayerActor : PlayerActor
{
    public static event System.Action<PlayerController> OnReadyToTakePass;

    //BallController ball;
    PlayerController playerController;
    //float ballControllRange;
    //TeamController team, enemyTeam;
    float oldStoppingDistance;

    public override void Start ()
    {
        base.Start();

        //ball = GameManager.Instance.Ball;
        playerController = GetComponent<PlayerController>();
        PassPlayerActor.OnReadyToMakePass += OnReadyToMakePass;
        //ballControllRange = playerController.BallControllRange;
        //team = playerController.Team;
        //enemyTeam = GameManager.Instance.GetEnemyTeam(team);

        //ModelAnimationEventRepeater eventRepeater = GetComponentInChildren<ModelAnimationEventRepeater>();
        //if (eventRepeater != null)
        //    eventRepeater.OnStandingUpInAnimation += OnStandingUp;

    }

    public override void Update ()
    {
        if (onPause || !isPlaying)
            return;

        base.Update();

        if (!onPause)
        {
            animator.SetFloat("Speed", navAgent.velocity.magnitude);
        }

        //if (Vector3.Distance(transform.position, targetPosition) < 0.1f)
        //{
        //    state = EActorState.AfterAction;
        //    End();
        //}

        //if (targetPosition != Vector3.zero && Vector3.Distance(transform.position, targetPosition) < ballControllRange)
        //{
        //    if(enemyTeam.IsControllBall())
        //    {
        //        // запускаем борьбу за мяч
        //    }
        //    else
        //    {
        //        state = EActorState.AfterAction;
        //        playerController.TakeBall();
        //        print(name + ": Tackle action ended ");
        //        End();
        //    }
        //}
    }

    private void LateUpdate()
    {
        //if (onPause || !isPlaying)
        //    return;

        //targetPosition = ball.transform.position;
        //navAgent.destination = targetPosition;
    }

    public override void Init()
    {
        
    }

    public override void Play()
    {
        print(name + ": Take pass action started ");
        isPlaying = true;
        navAgent.enabled = true;
        targetPosition = transform.position + transform.forward * 5 ;
        navAgent.destination = targetPosition;
        //navAgent.speed = 3.5f;
        state = EActorState.BeforeAction;

        //StartCoroutine(MakeRegistryPlayerReadyToTakePass());
    }

    IEnumerator MakeRegistryPlayerReadyToTakePass()
    {
        yield return new WaitForSeconds(0.05f);

        RegistryPlayerReadyToTakePass();

    }

    void RegistryPlayerReadyToTakePass()
    {
        if (OnReadyToTakePass != null)
            OnReadyToTakePass(playerController);
    }

    public override void End()
    {
        print(name + ": Take pass ended");
        base.End();
        if(oldStoppingDistance != 0)
            navAgent.stoppingDistance = oldStoppingDistance;
    }

    public override void Stop()
    {

        print(name + ": Take pass stoped");
        isPlaying = false;
        //navAgent.stoppingDistance = oldStoppingDistance;
        navAgent.enabled = false;
        animator.SetFloat("Speed", 0);
    }

    void OnBallTaked()
    {
        state = EActorState.AfterAction;
        print(name + ": Take pass action ended ");
        End();

    }

    public void SetTakePassPosition(Vector3 takePosition)
    {
        if (!isPlaying)
            return;

        state = EActorState.MakeAction;
        print(name + ": Set Take pass position " + takePosition);
        targetPosition = takePosition;
        navAgent.destination = targetPosition;
        oldStoppingDistance = navAgent.stoppingDistance;
        navAgent.stoppingDistance = 0;
    }

    void OnReadyToMakePass(TeamController team)
    {
        if (team != playerController.Team || !isPlaying)
            return;

        RegistryPlayerReadyToTakePass();
    }
}
