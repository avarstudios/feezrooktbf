﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassPlayerActor : PlayerActor
{
    public static event System.Action<TeamController> OnReadyToMakePass;
    List<PlayerController> playersReadyToTakePass;
    PlayerController currentPlayer;
    void Start ()
    {
        base.Start();
        startActionRange = 1;
        
        ModelAnimationEventRepeater eventRepeater = GetComponentInChildren<ModelAnimationEventRepeater>();
        currentPlayer = GetComponent<PlayerController>();
        TakePassPlayerActor.OnReadyToTakePass += RegistryPlayerReadyToTakePass;
        if (eventRepeater != null)
            eventRepeater.OnKickBallInAnimation += KickBall;
    }

    void Update ()
    {
        if (onPause || !isPlaying)
            return;

        base.Update();

        
        if(state == EActorState.MakeAction)
        //animator.SetFloat("MakePass", 1);
        { }
        else
            animator.SetFloat("Speed", navAgent.velocity.magnitude);

        if(state == EActorState.BeforeAction && Vector3.Distance(transform.position, targetPosition) < startActionRange)
        {
            print("Change state from BeforeAction to MakeAction in action Pass for player " + name);
            state = EActorState.MakeAction;
            animator.SetFloat("MakePass", 1);
            targetPosition = transform.position + transform.forward * 10;
            navAgent.destination = targetPosition;
        }

        if (state == EActorState.AfterAction && Vector3.Distance(transform.position, targetPosition) < startActionRange)
        {
            End();
        }
    }

    public override void Init()
    {
        
    }

    public override void Play()
    {
        print(name + ": Pass action started ");
        isPlaying = true;
        navAgent.enabled = true;
        targetPosition = passStartPosition;
        navAgent.destination = targetPosition;
        stoppingDistance = navAgent.stoppingDistance;
        navAgent.stoppingDistance = 0;
        state = EActorState.BeforeAction;
        playersReadyToTakePass = new List<PlayerController>();

        StartCoroutine(ReadyToMakePass());
    }

    public override void Stop()
    {
        print(name + ": Pass action stoped");
        isPlaying = false;
        navAgent.enabled = false;
        animator.SetFloat("Speed", 0);
    }

    public override void End()
    {
        navAgent.enabled = false;
        print(name + ": Pass action ended");
        base.End();

    }

    public void KickBall()
    {
        if (state == EActorState.AfterAction || !isPlaying)
            return;

        state = EActorState.AfterAction;

        print(name + " KickBall in action Pass for player " + name + " Ball move to pos: " + passTargetPosition);

        BallController ball = GameManager.Instance.Ball;
        ball.transform.position = transform.position + transform.forward * 1;

        Vector3 ballTargetPos = GetBallTargetPos();

        ball.Move(ballTargetPos, false, Speed);

        base.BallKicked();

        targetPosition = moveTargetPosition;
        navAgent.destination = targetPosition;
        navAgent.stoppingDistance = stoppingDistance;
        animator.SetFloat("MakePass", 0);
        print(name + " Change state from MakeAction to AfterAction in action Pass for player " + name + " Move to " + targetPosition);
        
    }

    IEnumerator ReadyToMakePass()
    {
        yield return new WaitForSeconds(0.05f);

        if (OnReadyToMakePass != null)
            OnReadyToMakePass(currentPlayer.Team);
    }

    void RegistryPlayerReadyToTakePass(PlayerController player)
    {
        if (!isPlaying || player.Team != currentPlayer.Team)
            return;

        print(name + " registry player " + player.name + " as ready to take pass");
        playersReadyToTakePass.Add(player);
    }

    Vector3 GetBallTargetPos()
    {
        Vector3 result = passTargetPosition;
        if (playersReadyToTakePass.Count == 0)
            return passTargetPosition;

        PlayerController chosenOne = null;
        float bestDistance = float.MaxValue;
        Vector3 currentPlayerPos = transform.position;
        foreach (var testedPlayer in playersReadyToTakePass)
        {
            Vector3 testedPlayerPos = testedPlayer.transform.position;
            if (GameMath.IsPointInZone(testedPlayerPos, activeZoneBorder1, activeZoneBorder2))
            {
                float currDistance = Vector3.Distance(testedPlayerPos, currentPlayerPos);
                if(currDistance < bestDistance)
                {
                    chosenOne = testedPlayer;
                    bestDistance = currDistance;
                }
            }
        }

        if(chosenOne == null)
        {
            print(name + " Take pass player not found. Pass to base point");
            return passTargetPosition;
        }

        

        Vector3 ballTargetPos = GetPositionForPass(chosenOne.transform.position);

        print(name + " Take pass player found - " + chosenOne.name + " pass maked in point " + ballTargetPos);

        //TakePassPlayerActor takePassActor = chosenOne.GetComponent<TakePassPlayerActor>();
        //if (takePassActor != null)
        //    takePassActor.SetTakePassPosition(ballTargetPos);
        TakePassPlayerActor takePassActor;
        foreach (var testedPlayer in playersReadyToTakePass)
        {
            takePassActor = chosenOne.GetComponent<TakePassPlayerActor>();
            if (takePassActor != null)
                takePassActor.SetTakePassPosition(ballTargetPos);
        }

        return ballTargetPos;
    }

    Vector3 GetPositionForPass(Vector3 chosenPlayerPos)
    {
        Vector3 gatesPos = GameManager.Instance.GetTeamGatesPos(GameManager.Instance.GetEnemyTeam(currentPlayer.Team));
        float currentDsitance = Vector3.Distance(currentPlayer.transform.position, chosenPlayerPos);
        float passShift = currentDsitance * 0.20f;
        if (passShift < 1)
            passShift = 1;

        Vector3 result = gatesPos - chosenPlayerPos;
        result.Normalize();
        result = chosenPlayerPos + result * passShift;

        return result;
    }
}
