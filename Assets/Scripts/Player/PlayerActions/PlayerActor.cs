﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum EActorState
{
    BeforeAction,
    MakeAction,
    AfterAction
}

public class PlayerActor : MonoBehaviour {

    public event System.Action<PlayerActor> OnActorEnded;
    public event System.Action OnBallKicked;
    public EPlayerAction action { get; set; }

    protected NavMeshAgent navAgent;

    protected Animator animator;
    public Vector3 targetPosition { get; set; }
    public Vector3 passStartPosition { get; set; }
    public Vector3 passTargetPosition { get; set; }
    public Vector3 moveTargetPosition { get; set; }
    public Vector3 activeZoneBorder1 { get; set; }
    public Vector3 activeZoneBorder2 { get; set; }
    public PlayerController passTarget { get; set; }
    public float Speed { get; set; }
    public float startActionRange { get; set; }
    protected float stoppingDistance = 1.5f;
    [SerializeField]
    protected bool onPause;
    protected EActorState state;
    [SerializeField]
    protected bool isPlaying;

    public bool TackleAnyCost { get; set; }
    public float TackleDistance { get; set; }
    public EActorState State { get { return state; } }

    public virtual void Start ()
    {
        animator = GetComponentInChildren<Animator>();
        navAgent = GetComponent<NavMeshAgent>();
    }

    public virtual void Update ()
    {
		
	}

    public virtual void Init()
    {
        
    }

    public virtual void Play()
    {
        isPlaying = true;
    }

    public virtual void Stop()
    {
        isPlaying = false;
    }

    public virtual void End()
    {
        isPlaying = false;

        if (OnActorEnded != null)
            OnActorEnded(this);
    }

    public virtual void Pause()
    {
        onPause = true;
        navAgent.enabled = false;
        animator.speed = 0;
    }

    public virtual void Resume()
    {
        onPause = false;
        navAgent.enabled = true;
        navAgent.destination = targetPosition;
        animator.speed = 1;
    }

    protected void BallKicked()
    {
        if (OnBallKicked != null)
            OnBallKicked();
    }
}
