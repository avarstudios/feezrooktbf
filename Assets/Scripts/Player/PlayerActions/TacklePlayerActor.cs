﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TacklePlayerActor : PlayerActor
{
    BallController ball;
    PlayerController playerController;
    float ballControllRange;
    TeamController team, enemyTeam;
    float oldStoppingDistance;

    public override void Start ()
    {
        base.Start();

        ball = GameManager.Instance.Ball;
        playerController = GetComponent<PlayerController>();
        ballControllRange = playerController.BallControllRange;
        team = playerController.Team;
        enemyTeam = GameManager.Instance.GetEnemyTeam(team);

        ModelAnimationEventRepeater eventRepeater = GetComponentInChildren<ModelAnimationEventRepeater>();
        if (eventRepeater != null)
            eventRepeater.OnStandingUpInAnimation += OnStandingUp;

    }

    public override void Update ()
    {
        if (onPause || !isPlaying)
            return;

        base.Update();

        if (!onPause)
        {
            animator.SetFloat("Speed", navAgent.velocity.magnitude);
        }

        if (targetPosition != Vector3.zero && Vector3.Distance(transform.position, targetPosition) < TackleDistance 
            && TackleAnyCost && enemyTeam.IsControllBall() && state == EActorState.BeforeAction)
        {
            // Если играем грубо - пора что-то предпринять
            state = EActorState.BeforeAction;
        }

        if (targetPosition != Vector3.zero && Vector3.Distance(transform.position, targetPosition) < ballControllRange)
        {
            if(enemyTeam.IsControllBall())
            {
                // запускаем борьбу за мяч
            }
            else
            {
                state = EActorState.AfterAction;
                playerController.TakeBall();
                print(name + ": Tackle action ended ");
                End();
            }
        }
    }

    private void LateUpdate()
    {
        if (onPause || !isPlaying)
            return;

        targetPosition = ball.transform.position;
        navAgent.destination = targetPosition;
    }

    public override void Init()
    {
        
    }

    public override void Play()
    {
        print(name + ": Tackle action started ");
        isPlaying = true;
        navAgent.enabled = true;
        targetPosition = ball.transform.position;
        navAgent.destination = targetPosition;
        oldStoppingDistance = navAgent.stoppingDistance;
        navAgent.stoppingDistance = 0;
        navAgent.speed = Speed;
        state = EActorState.BeforeAction;  
    }

    public override void Stop()
    {

        print(name + ": Tackle action stoped");
        isPlaying = false;
        navAgent.stoppingDistance = oldStoppingDistance;
        navAgent.enabled = false;
        animator.SetFloat("Speed", 0);
        
    }

    void OnStandingUp()
    {
        state = EActorState.AfterAction;
        print(name + ": Tackle action ended ");
        End();

    }
}
