﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EPlayerActionType
{
    Move,
    Defence,
    Pass,
    ShotOnGoal
}

public enum EPlayerAction
{
    Move,       // Двигаться
    Pass,       // Дать пас
    TakePass,   // Принять пас
    Shot,       // Ударить по воротам
    HighCross,    // Выполнить навес
    LowCross,    // Выполнить прострел
    MarkOn,     // Начать опеку
    Tackle,     // Выполнить отбор мяча
    TackleAnyCost // Выполнить отбор мяча в жестком стиле
}




public class PlayerAction
{

    //public EPlayerActionType Type { get; set; }

    public EPlayerAction Action { get; set; }
    public Vector3 StartPosition { get; set; }
    public Vector3 TargetPosition { get; set; }

    public Vector3 ActionZoneBorder1 { get; set; }
    public Vector3 ActionZoneBorder2 { get; set; }



    public static EPlayerActionType GetActionTypeByAction(EPlayerAction action)
    {
        switch (action)
        {
            case EPlayerAction.Move:
                return EPlayerActionType.Move;
                break;

            case EPlayerAction.Pass:
                return EPlayerActionType.Pass;
                break;

            case EPlayerAction.Shot:
                return EPlayerActionType.ShotOnGoal;
                break;

            case EPlayerAction.HighCross:
                return EPlayerActionType.Pass;
                break;

            case EPlayerAction.LowCross:
                return EPlayerActionType.Pass;
                break;

            case EPlayerAction.TakePass:
                return EPlayerActionType.Pass;
                break;

            case EPlayerAction.MarkOn:
                return EPlayerActionType.Defence;
                break;

            case EPlayerAction.Tackle:
                return EPlayerActionType.Defence;
                break;

            case EPlayerAction.TackleAnyCost:
                return EPlayerActionType.Defence;
                break;

            default:
                return EPlayerActionType.Move;
                break;
        }
    }

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
