﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum ECommandsToActionType
{
    MoveLeft1,
    MoveLeft2,
    MoveForward1,
    MoveForward2,
    MoveRight1,
    MoveRight2,
    MoveForwardAndLeft1,
    MoveForwardAndLeft2,
    MoveForwardAndRight1,
    MoveForwardAndRight2,
    MoveBack,
    Stop,
    ShotOnGoal,
    MakePass,
    InterceptBall,
    MarkOn
}




public class PlayerActionController : MonoBehaviour {

    public event System.Action OnActionEnded;
    public event System.Action OnBallKicked;
    PlayerActor moveActor;
    PlayerActor passActor;
    PlayerActor takePassActor;
    PlayerActor shotActor;
    PlayerActor tackleActor;




    PlayerActor currentActor;


    PlayerController playerController;
    BallController ball;
    NavMeshAgent navAgent;

    private float ballImpulceMaxDistance = 0.5f;
    private float ballImpulceCurrDistance;
    private float ballImpulceDirection = 1;
    private float ballImpulceSpeed = 2f;
    bool inPause;

    void Start ()
    {
        moveActor = gameObject.AddComponent<MovePlayerActor>();
        moveActor.OnActorEnded += OnActorEnded;

        passActor = gameObject.AddComponent<PassPlayerActor>();
        passActor.OnActorEnded += OnActorEnded;
        passActor.OnBallKicked += BallKicked;


        takePassActor = gameObject.AddComponent<TakePassPlayerActor>();
        takePassActor.OnActorEnded += OnActorEnded;
        //takePassActor.OnBallKicked += BallKicked;

        shotActor = gameObject.AddComponent<ShotPlayerActor>();
        shotActor.OnActorEnded += OnActorEnded;
        shotActor.OnBallKicked += BallKicked;

        tackleActor = gameObject.AddComponent<TacklePlayerActor>();
        tackleActor.OnActorEnded += OnActorEnded;

        playerController = GetComponent<PlayerController>();
        ball = GameManager.Instance.Ball;
        navAgent = GetComponent<NavMeshAgent>();
    }
	
	
	void Update ()
    {
        if (inPause)
            return;

		if(playerController.IsControllBall)
        {
            ball.transform.position = GetBallPosInLegs();
            ball.transform.Rotate(new Vector3(1, 1, 1), ball.RotationSpeed);

        }
    }

    public void Move(Vector3 targetPosition)
    {
        if (currentActor != null && !currentActor.Equals(moveActor))
            currentActor.Stop();
        
        moveActor.targetPosition = targetPosition;
        moveActor.Speed = 3.5f;
        moveActor.Play();
        currentActor = moveActor;
    }


    public void Pass(PlayerAction passAction, Vector3 passTargetPosition, Vector3 passStartPosition, Vector3 moveTargetPosition, float speed, float accurency)
    {

        if (currentActor != null && !currentActor.Equals(passActor))
            currentActor.Stop();

        passActor.passTargetPosition = passTargetPosition;
        passActor.passStartPosition = passStartPosition;
        passActor.moveTargetPosition = moveTargetPosition;
        passActor.activeZoneBorder1 = passAction.ActionZoneBorder1;
        passActor.activeZoneBorder2 = passAction.ActionZoneBorder2;
        passActor.Speed = speed;

        print("Pass play started. Params: passTargetPosition = " + passTargetPosition + " passStartPosition = " + passStartPosition + " moveTargetPosition = " + moveTargetPosition + " speed = " + speed);

        passActor.Play();
        currentActor = passActor;

    }

    public void TakePass(PlayerAction passAction)
    {

        if (currentActor != null && !currentActor.Equals(takePassActor))
            currentActor.Stop();

        //takePassActor.Speed = speed;

        print("Take pass play started.");

        takePassActor.Play();
        currentActor = takePassActor;

    }

    public void Shot(Vector3 passTargetPosition, Vector3 passStartPosition, Vector3 moveTargetPosition, float speed, float accurency)
    {

        if (currentActor != null && !currentActor.Equals(shotActor))
            currentActor.Stop();

        shotActor.passTargetPosition = passTargetPosition;
        shotActor.passStartPosition = passStartPosition;
        shotActor.moveTargetPosition = moveTargetPosition;
        shotActor.Speed = speed;

        print("Shot play started. Params: passTargetPosition = " + passTargetPosition + " passStartPosition = " + passStartPosition + " moveTargetPosition = " + moveTargetPosition + " speed = " + speed);

        shotActor.Play();
        currentActor = shotActor;

    }

    public void Tackle(bool byAnyCost)
    {
        if (currentActor != null && !currentActor.Equals(tackleActor))
            currentActor.Stop();

        tackleActor.TackleAnyCost = byAnyCost;
        tackleActor.Speed = 3.5f;
        tackleActor.TackleDistance = 3;
        tackleActor.Play();
        currentActor = tackleActor;
    }

    public void Pause()
    {
        inPause = true;

        if (currentActor != null)
            currentActor.Pause();
    }

    public void Resume()
    {
        inPause = false;

        if (currentActor != null)
            currentActor.Resume();
    }

    public void StartNewAction(PlayerAction newAction)
    {
        print("Player " + name + " start new action " + newAction.Action);
        switch (newAction.Action)
        {
            case EPlayerAction.Move:
                Move(newAction.TargetPosition);
                break;

            case EPlayerAction.Pass:
                //Pass(newAction.TargetPosition, transform.position + transform.forward * 3, transform.position + transform.forward * 6, 20, 1);
                Pass(newAction, newAction.TargetPosition, transform.position + transform.forward * 3, transform.position + transform.forward * 6, 20, 1);
                break;

            case EPlayerAction.TakePass:
                TakePass(newAction);
                break;

            case EPlayerAction.Shot:
                TeamController enemyTeam = GameManager.Instance.GetEnemyTeam(playerController.Team);
                Vector3 goalPos = GameManager.Instance.GetTeamGatesPos(enemyTeam);
                Shot(goalPos, transform.position + transform.forward * 3, transform.position + transform.forward * 6, 30, 1);
                break;

            case EPlayerAction.HighCross:
                break;
            case EPlayerAction.LowCross:
                break;
            case EPlayerAction.MarkOn:
                break;

            case EPlayerAction.Tackle:
                Tackle(false);
                break;

            case EPlayerAction.TackleAnyCost:
                Tackle(true);
                break;

            default:
                print("Unknown action " + newAction.Action);
                break;
        }
    }

    void OnActorEnded(PlayerActor actor)
    {
        if (currentActor != null)
            currentActor.Stop();

        if (actor != currentActor && actor != null)
            actor.Stop();

        if (OnActionEnded != null)
            OnActionEnded();
    }

    void BallKicked()
    {
        if (OnBallKicked != null)
            OnBallKicked();
    }

    Vector3 GetBallPosInLegs()
    {
        float BallControllRange = playerController.BallControllRange;
        Vector3 ballPos = BallControllRange * transform.forward + transform.position;
        // проверяем, не пора ли мячу двигаться в другую сторону
        if (ballImpulceCurrDistance > (BallControllRange - 0.2f) || ballImpulceCurrDistance < (BallControllRange - ballImpulceMaxDistance))
            ballImpulceDirection = -ballImpulceDirection;

        // фиксируем граничные условия
        if (ballImpulceCurrDistance > (BallControllRange - 0.2f))
            ballImpulceCurrDistance = (BallControllRange - 0.2f);
        else if (ballImpulceCurrDistance < (BallControllRange - ballImpulceMaxDistance))
            ballImpulceCurrDistance = (BallControllRange - ballImpulceMaxDistance);

        if(navAgent.velocity.magnitude > 0.1f)
        {
            ballImpulceCurrDistance += ballImpulceSpeed * Time.deltaTime * ballImpulceDirection;
        }

        ballPos = (BallControllRange + ballImpulceCurrDistance) * transform.forward + transform.position;
        
        ballPos = new Vector3(ballPos.x, GameManager.Instance.Ball.MinY, ballPos.z);

        return ballPos;
    }


}
