﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotPlayerActor : PlayerActor
{
    void Start ()
    {
        base.Start();
        startActionRange = 1;

        ModelAnimationEventRepeater eventRepeater = GetComponentInChildren<ModelAnimationEventRepeater>();
        if (eventRepeater != null)
            eventRepeater.OnKickBallInAnimation += KickBall;
    }

    void Update ()
    {
        if (onPause || !isPlaying)
            return;

        base.Update();

        
        if(state == EActorState.MakeAction)
        {
        }
        else
            animator.SetFloat("Speed", navAgent.velocity.magnitude);

        if(state == EActorState.BeforeAction && Vector3.Distance(transform.position, targetPosition) < startActionRange)
        {
            print("Change state from BeforeAction to MakeAction in action Shot for player " + name);
            state = EActorState.MakeAction;
            animator.SetFloat("MakeShot", 1);
            targetPosition = transform.position + transform.forward * 10;
            navAgent.destination = targetPosition;
        }

        if (state == EActorState.AfterAction && Vector3.Distance(transform.position, targetPosition) < startActionRange)
        {
            End();
        }
    }

    public override void Init()
    {
        
    }

    public override void Play()
    {
        print(name + ": Shot action started ");
        isPlaying = true;
        navAgent.enabled = true;
        targetPosition = passStartPosition;
        navAgent.destination = targetPosition;
        stoppingDistance = navAgent.stoppingDistance;
        navAgent.stoppingDistance = 0;
        state = EActorState.BeforeAction;
    }

    public override void Stop()
    {
        print(name + ": Shot action stoped");
        isPlaying = false;
        navAgent.enabled = false;
        animator.SetFloat("Speed", 0);
    }

    public override void End()
    {
        navAgent.enabled = false;
        print(name + ": Shot action ended");
        base.End();

    }

    public void KickBall()
    {
        if (state == EActorState.AfterAction || !isPlaying)
            return;

        state = EActorState.AfterAction;

        print(name + " KickBall in action Shot for player " + name + " Ball move to pos: " + passTargetPosition);

        BallController ball = GameManager.Instance.Ball;
        ball.transform.position = transform.position + transform.forward * 1;
        ball.Move(passTargetPosition, false, Speed);

        base.BallKicked();

        targetPosition = moveTargetPosition;
        navAgent.destination = targetPosition;
        navAgent.stoppingDistance = stoppingDistance;
        animator.SetFloat("MakeShot", 0);
        print(name + " Change state from MakeAction to AfterAction in action Shot for player " + name + " Move to " + targetPosition);
        
    }
}
