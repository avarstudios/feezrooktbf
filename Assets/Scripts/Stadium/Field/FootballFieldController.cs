﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootballFieldController : MonoBehaviour {

    public static event System.Action<PlayerController> OnMouseSelected;

    [SerializeField]
    Transform goalsPosition1;

    [SerializeField]
    Transform goalsPosition2;

    [SerializeField]
    Transform fieldBorder1;

    [SerializeField]
    Transform fieldBorder2;

    private static FootballFieldController footballFieldController;
    public static Transform GoalsPosition1
    {
        get { return footballFieldController.goalsPosition1; }
    }
    public static Transform GoalsPosition2
    {
        get { return footballFieldController.goalsPosition2; }
    }

    public static Transform FieldBorder1
    {
        get { return footballFieldController.fieldBorder1; }
    }
    public static Transform FieldBorder2
    {
        get { return footballFieldController.fieldBorder2; }
    }

    void Start () {
        footballFieldController = this;

    }
	
	void Update () {
		
	}

    private void OnMouseDown()
    {
        //if (OnMouseSelected != null)
        //    OnMouseSelected(null);
    }
}
