﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

    [SerializeField]
    float baseSpeed = 10;

    [SerializeField]
    float minSpeed = 0.1f;

    [SerializeField]
    float baseDrag = 0.5f;

    [SerializeField]
    float minY = 0.5f;
    public float MinY
    {
        get
        {
            return minY;
        }
    }

    [SerializeField]
    float rotationSpeed = 1.1f;
    public float RotationSpeed
    {
        get
        {
            return rotationSpeed;
        }
    }


    Vector3 velosity;
    float currentSpeed;
    Vector3 targetPosition;

    bool isPaused;



    bool isMoving;

    void Start ()
    {
        GameManager.OnCommandPhaseStarted += Pause;
        GameManager.OnPlayPhaseStarted += Resume;
    }
	
	void Update ()
    {
		if(isMoving && !isPaused)
        {
            transform.Rotate(new Vector3(1, 1, 1), rotationSpeed);

            transform.position = transform.position + velosity * currentSpeed * Time.deltaTime;
            if (transform.position.y <= minY)
                transform.position = new Vector3(transform.position.x, minY, transform.position.z);

            currentSpeed = currentSpeed - baseDrag * Time.deltaTime;

            if (currentSpeed < minSpeed)
                Stop();
        }
	}

    public void Move(Vector3 targetPosition, bool hiShot = false, float startSpeed = 0)
    {
        isMoving = true;

        currentSpeed = startSpeed;
        if (startSpeed == 0)
            currentSpeed = baseSpeed;

        velosity = targetPosition - transform.position;
        velosity.Normalize();

    }

    public void Stop()
    {
        isMoving = false;
        velosity = Vector3.zero;
        currentSpeed = 0;
    }
    public void Pause()
    {
        isPaused = true;
    }

    public void Resume()
    {
        isPaused = false;
    }
}
