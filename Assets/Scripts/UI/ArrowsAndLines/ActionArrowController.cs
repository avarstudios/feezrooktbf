﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ActionArrowController : MonoBehaviour {

    [SerializeField]
    private GameObject arrowBody;
    [SerializeField]
    private float arrowBodyLength = 1.25f;
    [SerializeField]
    private GameObject arrowHead;



    public Vector3 startPos { get; set; }
    public Vector3 endPos { get; set; }
    public EPlayerActionType actionType { get; set; }

    public EPlayerAction action { get; set; }

    private void Start()
    {
        actionType = EPlayerActionType.Move;
        
    }

    public void Show(Vector3 startPos, Vector3 endPos)
    {
        this.startPos = startPos;
        this.endPos = endPos;
        Show();
    }

    public void Show(EPlayerActionType actionType, Vector3 startPos, Vector3 endPos)
    {
        this.actionType = actionType;
        Show(startPos, endPos);
    }

    public void Show(EPlayerAction action, Vector3 startPos, Vector3 endPos)
    {
        this.action = action;
        Show(startPos, endPos);
    }

    public void Show()
    {
        Vector3 arrowDirection = endPos - startPos;
        float arrowLenght = arrowDirection.magnitude;
        arrowDirection.Normalize();

        int bodyElements = (int)(arrowLenght / arrowBodyLength);
        Vector3 currPos = startPos + arrowDirection;
        GameObject newBodyElement;
        for (int i = 1; i < bodyElements-1; i++)
        {
            newBodyElement = Instantiate<GameObject>(arrowBody);
            newBodyElement.transform.position = currPos;
            newBodyElement.transform.LookAt(startPos);
            newBodyElement.GetComponent<MeshRenderer>().sharedMaterial = GamePrefabs.GetMaterialByAction(action);
            newBodyElement.SetActive(true);
            newBodyElement.transform.SetParent(transform);
            currPos = currPos + arrowDirection;
        }
        newBodyElement = Instantiate<GameObject>(arrowHead);
        newBodyElement.GetComponent<MeshRenderer>().sharedMaterial = GamePrefabs.GetMaterialByAction(action);
        newBodyElement.transform.position = currPos;
        newBodyElement.transform.LookAt(startPos);
        newBodyElement.SetActive(true);
        newBodyElement.transform.SetParent(transform);

    }

    public void Hide()
    {
        Destroy(gameObject);
    }
}
