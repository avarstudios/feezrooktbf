﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerOnMapIndicatorController : MonoBehaviour {

    public Color color { get; set;  }
    public string number { get; set; }

    [SerializeField]
    Text text;

    [SerializeField]
    Image image;

    [SerializeField]
    Image selectionMarker;

    LineRenderer lineRenderer;

    [SerializeField]
    GameObject linePrefab;

    List<GameObject> lineElements = new List<GameObject>();

    void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    public void UpdateInfo(Color color, string number, bool isSelectedPlayer = false)
    {
        text.text = number;
        text.color = color;

        image.color = color;

        selectionMarker.enabled = isSelectedPlayer;
        //image.sprite.;
    }

    public void DrawLine(Vector2 startPos, Vector2 endPos, Color color, bool isFirstAction = false)
    {
        float distance = Vector2.Distance(startPos, endPos);
        float lineElementLenght = 10f;
        float scaleX = distance / lineElementLenght;

        Vector2 direction = endPos - startPos;
        direction.Normalize();
        float delay = selectionMarker.rectTransform.rect.width;
        float speed = lineElementLenght;
        //Vector2 currPos = startPos + direction * delay;
        Vector2 currPos = startPos + direction * speed;
        if(isFirstAction)
            currPos = startPos + direction * delay;

        while (Vector2.Distance(currPos, endPos) > speed)
        {
            GameObject line = Instantiate<GameObject>(linePrefab);
            line.transform.SetParent(transform);
            line.transform.position = currPos;
            line.GetComponent<Image>().color = color;
            lineElements.Add(line);

            currPos = currPos + direction * speed;

        }

    }
    public void ClearLines()
    {
        foreach (var item in lineElements)
        {
            Destroy(item);
        }
    }

    void OnDisable()
    {
        ClearLines();
    }

}
