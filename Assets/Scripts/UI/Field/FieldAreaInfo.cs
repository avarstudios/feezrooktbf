﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldAreaInfo : MonoBehaviour {

    public static event System.Action<FieldAreaInfo> OnFieldAreaSelected;

    public int AreaID;

    public void OnClick()
    {
        if (OnFieldAreaSelected != null)
            OnFieldAreaSelected(this);

    }
}
