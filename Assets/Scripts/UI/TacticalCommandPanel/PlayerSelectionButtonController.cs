﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSelectionButtonController : MonoBehaviour {

    [SerializeField]
    bool isSelectNextPlayer;

    PlayerController currentPlayer;

    [SerializeField]
    Text CurrentPlayerNameIndicator;

	// Use this for initialization
	void Start () {
        PlayerController.OnMouseSelected += OnPlayerSelected;
    }
	
    public void OnClick()
    {
        if (isSelectNextPlayer)
            currentPlayer.Team.GetNextPlayer(currentPlayer).OnPlayerSelected();
        else
            currentPlayer.Team.GetPreviousPlayer(currentPlayer).OnPlayerSelected();
    }

    public void OnPlayerSelected(PlayerController newPlayer)
    {
        currentPlayer = newPlayer;
        CurrentPlayerNameIndicator.text = currentPlayer.name;
    }
}
