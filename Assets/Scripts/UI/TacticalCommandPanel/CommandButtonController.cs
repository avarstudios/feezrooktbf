﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommandButtonController : MonoBehaviour {

    public EPlayerAction CommandToAction;

    public static event System.Action<CommandButtonController> OnCommandButtonClicked;

    [SerializeField]
    Sprite OnPressedInage;

    [SerializeField]
    Sprite OnReleasedInage;

    Image buttonImage;

    // Use this for initialization
    void Start () {
        buttonImage = GetComponent<Image>();
        buttonImage.sprite = OnReleasedInage;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick()
    {
        if (OnCommandButtonClicked != null)
            OnCommandButtonClicked(this);
    }

    public void Fix()
    {
        buttonImage.sprite = OnPressedInage;
    }

    public void Unfix()
    {
        buttonImage.sprite = OnReleasedInage;
    }

}
