﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TacticalCommandsPanelController : MonoBehaviour {

    [SerializeField]
    CommandButtonController CommandHighCross;
    [SerializeField]
    CommandButtonController CommandShot;
    [SerializeField]
    CommandButtonController CommandMarkOn;
    [SerializeField]
    CommandButtonController CommandPass;
    [SerializeField]
    CommandButtonController CommandTackle;
    [SerializeField]
    CommandButtonController CommandLowCross;
    [SerializeField]
    CommandButtonController CommandAnyCost;
    [SerializeField]
    CommandButtonController CommandTakePass;

    [SerializeField]
    SelectPlayerPanelController SelectPlayerPanel;

    private PlayerController currentPlayer { get; set; }

    public PlayerController CurrentPlayer
    {
        get { return currentPlayer; }
        set
        {
            currentPlayer = value;
            UpdatePlayersOnMap();
            //ShowCommandButtons();
        }
    }

    List<PlayerOnMapIndicatorController> playersIndicators = new List<PlayerOnMapIndicatorController>();

    [SerializeField]
    Image mapBackground;

    private bool isCommandQualification;
    private CommandButtonController currentCommand;
    private FieldAreaInfo currentFieldAreaInfo;

    private static TacticalCommandsPanelController instance;

    public static Vector2 MapCenter
    {
        get
        {
            Vector2 mapPos = instance.mapBackground.rectTransform.position;
            return mapPos;
        }
    }

    public static float MapWidth
    {
        get
        {
            return instance.mapBackground.rectTransform.rect.width;
        }
    }

    public static float MapHeight
    {
        get
        {
            return instance.mapBackground.rectTransform.rect.height;
        }
    }

    void Start ()
    {
        instance = this;
        FieldAreaInfo.OnFieldAreaSelected += OnFieldAreaSelected;
        CommandButtonController.OnCommandButtonClicked += OnCommandSelected;
        HideCommandButtons();
        SelectPlayerPanel.Hide();
        SelectPlayerPanel.OnSelectCanceled += OnPlayerSelectCanceled;
    }
	
	void Update ()
    {
		
	}

    public void OnFieldAreaSelected(FieldAreaInfo selectedArea)
    {
        if(isCommandQualification)
        {
            if(currentCommand.CommandToAction == EPlayerAction.Pass)
            {
                currentFieldAreaInfo = selectedArea;
                SelectPlayerPanel.team = currentPlayer.Team;
                SelectPlayerPanel.searchZoneBorder1 = GameManager.Instance.GetBorderPositionInFieldArea(selectedArea, true);
                SelectPlayerPanel.searchZoneBorder2 = GameManager.Instance.GetBorderPositionInFieldArea(selectedArea, false);
                SelectPlayerPanel.OnSelectEnded += OnPassPlayerSelected;
                SelectPlayerPanel.Show();
            }
            else if (currentCommand.CommandToAction == EPlayerAction.MarkOn)
            {
                currentFieldAreaInfo = selectedArea;
                SelectPlayerPanel.team = GameManager.Instance.GetEnemyTeam(currentPlayer.Team);
                SelectPlayerPanel.searchZoneBorder1 = GameManager.Instance.GetBorderPositionInFieldArea(selectedArea, true);
                SelectPlayerPanel.searchZoneBorder2 = GameManager.Instance.GetBorderPositionInFieldArea(selectedArea, false);
                SelectPlayerPanel.OnSelectEnded += OnMarkOnPlayerSelected;
                SelectPlayerPanel.Show();
            }
            else
            {
                currentPlayer.SetAction(currentCommand.CommandToAction, GameManager.Instance.GetPositionInFieldArea(selectedArea),
                                        GameManager.Instance.GetBorderPositionInFieldArea(selectedArea, true),
                                        GameManager.Instance.GetBorderPositionInFieldArea(selectedArea, false));
                isCommandQualification = false;
                currentCommand.Unfix();
                currentCommand = null;
            }
        }
        else
        {
            currentPlayer.SetTargetMovePosition(GameManager.Instance.GetPositionInFieldArea(selectedArea));
        }

        UpdatePlayersOnMap();
        
    }

    public void OnCommandSelected(CommandButtonController commandButton)
    {
        currentCommand = commandButton;
        print("New command: " + commandButton.CommandToAction + " for player " + currentPlayer.name);

        if(commandButton.CommandToAction == EPlayerAction.TackleAnyCost 
            || commandButton.CommandToAction == EPlayerAction.Tackle
            //|| commandButton.CommandToAction == EPlayerAction.Shot
            || commandButton.CommandToAction == EPlayerAction.TakePass)
        {
            currentPlayer.SetAction(commandButton.CommandToAction);
            isCommandQualification = false;
            currentCommand = null;
            UpdatePlayersOnMap();
        }
        else
        {
            isCommandQualification = true;
            currentCommand.Fix();
        }
    }

    void OnPassPlayerSelected(PlayerController selectedPlayer)
    {
        SelectPlayerPanel.OnSelectEnded -= OnPassPlayerSelected;
        print("Select target for pass - " + selectedPlayer);
        currentPlayer.SetAction(EPlayerAction.Pass, GameManager.Instance.GetPositionInFieldArea(currentFieldAreaInfo),
                        GameManager.Instance.GetBorderPositionInFieldArea(currentFieldAreaInfo, true),
                        GameManager.Instance.GetBorderPositionInFieldArea(currentFieldAreaInfo, false),
                        selectedPlayer);
        isCommandQualification = false;
        currentCommand.Unfix();
        currentCommand = null;
        currentFieldAreaInfo = null;
    }

    void OnMarkOnPlayerSelected(PlayerController selectedPlayer)
    {
        SelectPlayerPanel.OnSelectEnded -= OnMarkOnPlayerSelected;
        print("Select target for markOn - " + selectedPlayer);
        currentPlayer.SetAction(EPlayerAction.MarkOn, GameManager.Instance.GetPositionInFieldArea(currentFieldAreaInfo),
                GameManager.Instance.GetBorderPositionInFieldArea(currentFieldAreaInfo, true),
                GameManager.Instance.GetBorderPositionInFieldArea(currentFieldAreaInfo, false),
                selectedPlayer);
        isCommandQualification = false;
        currentCommand.Unfix();
        currentCommand = null;
        currentFieldAreaInfo = null;

    }

    void OnPlayerSelectCanceled()
    {
        isCommandQualification = false;
        if(currentCommand!=null)
            currentCommand.Unfix();
        currentCommand = null;
        currentFieldAreaInfo = null;
    }

    void UpdatePlayersOnMap()
    {
        TeamController playerTeam = currentPlayer.Team;
        TeamController enemyTeam = currentPlayer.Team == GameManager.Instance.Team1 ? GameManager.Instance.Team2 : GameManager.Instance.Team1;

        if(playerTeam == GameManager.Instance.CurrentCoatchTeam)
        {
            ClearIndicators();

            AddIndicatorsForTeam(enemyTeam);
            AddIndicatorsForTeam(playerTeam);
        }
        ShowCommandButtons();
    }

    void AddIndicatorsForTeam(TeamController currTeam)
    {
        bool showActions = currTeam == GameManager.Instance.CurrentCoatchTeam;

        foreach (var player in currTeam.Players)
        {
            if (player == null)
                continue;

            GameObject newIndicator = Instantiate<GameObject>(GamePrefabs.Instance.playerOnMapIndicatorPrefab);
            newIndicator.transform.SetParent(transform);

            newIndicator.transform.position = GameMath.GetPositionOnMap(player.transform.position);

            PlayerOnMapIndicatorController newIndicatorController = newIndicator.GetComponent<PlayerOnMapIndicatorController>();

            newIndicatorController.color = player.Team.TeamColor;
            newIndicatorController.number = "11";
            newIndicatorController.UpdateInfo(currTeam.TeamColor, player.Number, player == currentPlayer);
            //if(player.Team == currTeam)
            if(showActions)
            {
                //newIndicatorController.DrawLine(newIndicator.transform.position, GameMath.GetPositionOnMap(player.TargetPos), GamePrefabs.Instance.MoveArrowColor);
                if (player.Action1 != null)
                    newIndicatorController.DrawLine(GameMath.GetPositionOnMap(player.transform.position), GameMath.GetPositionOnMap(player.Action1.TargetPosition), GamePrefabs.GetColorByAction(player.Action1.Action), true);
                if (player.Action2 != null)
                    newIndicatorController.DrawLine(GameMath.GetPositionOnMap(player.Action2.StartPosition), GameMath.GetPositionOnMap(player.Action2.TargetPosition), GamePrefabs.GetColorByAction(player.Action2.Action));
            }

            playersIndicators.Add(newIndicatorController);
        }

        if(currTeam.Goalkeeper != null)
        {
            GameObject newIndicator = Instantiate<GameObject>(GamePrefabs.Instance.playerOnMapIndicatorPrefab);
            newIndicator.transform.SetParent(transform);

            newIndicator.transform.position = GameMath.GetPositionOnMap(currTeam.Goalkeeper.transform.position);

            PlayerOnMapIndicatorController newIndicatorController = newIndicator.GetComponent<PlayerOnMapIndicatorController>();

            newIndicatorController.UpdateInfo(currTeam.TeamColor, currTeam.Goalkeeper.Number, currTeam.Goalkeeper == currentPlayer);
            //if (currentPlayer.Team == currTeam)
            if(showActions)
            {
                //newIndicatorController.DrawLine(newIndicator.transform.position, GameMath.GetPositionOnMap(currTeam.Goalkeeper.TargetPos), GamePrefabs.Instance.MoveArrowColor);
                if (currTeam.Goalkeeper.Action1 != null)
                    newIndicatorController.DrawLine(GameMath.GetPositionOnMap(currTeam.Goalkeeper.transform.position), GameMath.GetPositionOnMap(currTeam.Goalkeeper.Action1.TargetPosition), GamePrefabs.GetColorByAction(currTeam.Goalkeeper.Action1.Action), true);
                if (currTeam.Goalkeeper.Action2 != null)
                    newIndicatorController.DrawLine(GameMath.GetPositionOnMap(currTeam.Goalkeeper.Action2.StartPosition), GameMath.GetPositionOnMap(currTeam.Goalkeeper.Action2.TargetPosition), GamePrefabs.GetColorByAction(currTeam.Goalkeeper.Action2.Action));

            }

            playersIndicators.Add(newIndicatorController);

        }
    }

    void ClearIndicators()
    {
        foreach (var item in playersIndicators)
        {
            Destroy(item.gameObject);
        }

        playersIndicators.Clear();
    }

    void HideCommandButtons()
    {
        CommandHighCross.gameObject.SetActive(false);
        CommandShot.gameObject.SetActive(false);
        CommandMarkOn.gameObject.SetActive(false);
        CommandPass.gameObject.SetActive(false);
        CommandTakePass.gameObject.SetActive(false);
        CommandTackle.gameObject.SetActive(false);
        CommandLowCross.gameObject.SetActive(false);
        CommandAnyCost.gameObject.SetActive(false);
    }

    void ShowCommandButtons()
    {
        HideCommandButtons();

        if (currentPlayer == null)
            return;

        if(currentPlayer.IsControllBall)
        {
            CommandShot.gameObject.SetActive(true);
            CommandPass.gameObject.SetActive(true);
        }
        else
        {
            bool isTeamControllBall = currentPlayer.Team.IsControllBall();
            if (!isTeamControllBall)
                CommandMarkOn.gameObject.SetActive(true);
            else if(!currentPlayer.IsControllBall)
                CommandTakePass.gameObject.SetActive(true);

            if (!isTeamControllBall && currentPlayer.IsBallInReactionRange(includeFuturePositions: true))
            {
                CommandTackle.gameObject.SetActive(true);
                CommandAnyCost.gameObject.SetActive(true);
            }
        }

        //CommandHighCross.gameObject.SetActive(false);
        //CommandMarkOn.gameObject.SetActive(false);
        //CommandTackle.gameObject.SetActive(false);
        //CommandLowCross.gameObject.SetActive(false);
        //CommandAnyCost.gameObject.SetActive(false);
    }
}
