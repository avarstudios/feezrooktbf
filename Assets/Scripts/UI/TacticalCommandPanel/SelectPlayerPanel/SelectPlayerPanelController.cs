﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectPlayerPanelController : MonoBehaviour {
    [SerializeField]
    SelectPlayerPanelButton[] avaiblePlayers;
    int maxAvaiblePlayers = 9;

    public event System.Action OnSelectCanceled;
    public event System.Action<PlayerController> OnSelectEnded;


    public Vector3 searchZoneBorder1 { get; set; }
    public Vector3 searchZoneBorder2 { get; set; }
    public Vector3 searchZoneCenter { get; set; }
    public TeamController team { get; set; }

    // Use this for initialization
    void Start ()
    {
        SelectPlayerPanelButton.OnPlayerSelected += Selected;

        SelectPlayerPanelButton[] btns = GetComponentsInChildren<SelectPlayerPanelButton>();
        int btnsCount = Mathf.Min(maxAvaiblePlayers, btns.Length);
        avaiblePlayers = new SelectPlayerPanelButton[btnsCount];
        for (int i = 0; i < btnsCount; i++)
        {
            avaiblePlayers[i] = btns[i];
            avaiblePlayers[i].Hide();
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}


    public void Cancel()
    {
        if (OnSelectCanceled != null)
            OnSelectCanceled();
        Hide();
    }

    public void Selected(SelectPlayerPanelButton selectedButton)
    {
        if (OnSelectEnded != null)
            OnSelectEnded(selectedButton.player);
        Hide();
    }

    public void Show()
    {
        searchZoneCenter = new Vector3((searchZoneBorder1.x + searchZoneBorder2.x)/2, 0, (searchZoneBorder1.z + searchZoneBorder2.z)/2);
        gameObject.SetActive(true);

        GetAvaiblePlayers();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    void GetAvaiblePlayers()
    {
        List<PlayerController> playersInArea = team.GetPlayersInArea(searchZoneBorder1, searchZoneBorder2);

        print("Players in avaible zone - " + playersInArea.Count);

        int playersCount = 0;
        int maxPlayers = avaiblePlayers.Length-1;
        foreach (var player in playersInArea)
        {
            avaiblePlayers[playersCount].player = player;
            avaiblePlayers[playersCount].Show();
            playersCount++;
        }
        for (int i = playersCount; i < maxPlayers; i++)
        {
            avaiblePlayers[i].player = null;
            avaiblePlayers[playersCount].Show();
        }
    }
}
