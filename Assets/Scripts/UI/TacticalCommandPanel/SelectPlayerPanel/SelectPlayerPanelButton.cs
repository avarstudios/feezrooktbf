﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectPlayerPanelButton : MonoBehaviour {

    public static event System.Action<SelectPlayerPanelButton> OnPlayerSelected;

    
    private Text numberField;
    
    private Image circle;


    public PlayerController player { get; set; }

    void Start ()
    {
        numberField = GetComponentInChildren<Text>();
        circle = GetComponent<Image>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Show()
    {
        print(name + " - " + player);
        if (player != null)
        {
            if(numberField == null)
                numberField = GetComponentInChildren<Text>();

            numberField.text = player.Number;
            numberField.color = player.Team.TeamColor;

            gameObject.SetActive(true);
        }
        else
            Hide();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }


    public void OnClick()
    {
        if (OnPlayerSelected != null)
            OnPlayerSelected(this);
    }
}
