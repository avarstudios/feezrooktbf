﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndTurnButtonController : MonoBehaviour {

    [SerializeField]
    Sprite PlaySprite;

    [SerializeField]
    Sprite PauseSprite;

    [SerializeField]
    Button Button;


    // Use this for initialization
    void Start () {
        GameManager.OnCommandPhaseStarted += OnCommandPhaseStarted;
        GameManager.OnPlayPhaseStarted += OnPlayPhaseStarted;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnCommandPhaseStarted()
    {
        Button.GetComponent<Image>().sprite = PlaySprite;
    }
    public void OnPlayPhaseStarted()
    {
        Button.GetComponent<Image>().sprite = PauseSprite;
    }
}
