﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionsPanelController : MonoBehaviour {

    // Use this for initialization

    private PlayerController currentPlayer;

    public PlayerController CurrentPlayer
    {
        get { return currentPlayer;  }
        set {
            if (value == currentPlayer)
                return;
            currentPlayer = value;
            UpdatePlayersOnMap();
        }
    }

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void UpdatePlayersOnMap()
    {

    }

    public void OnMoveForwardSelected(int commandID)
    {
        if (commandID == 1)
            OnCommandSelected(ECommandsToActionType.MoveForward1);
        else if (commandID == 2)
            OnCommandSelected(ECommandsToActionType.MoveForward2);
        else if (commandID == -1)
            OnCommandSelected(ECommandsToActionType.MoveBack);
    }

    public void OnMoveSideSelected(int commandID)
    {
        if (commandID == 1)
            OnCommandSelected(ECommandsToActionType.MoveRight1);
        else if (commandID == 2)
            OnCommandSelected(ECommandsToActionType.MoveRight2);
        else if (commandID == -1)
            OnCommandSelected(ECommandsToActionType.MoveLeft1);
        else if (commandID == -2)
            OnCommandSelected(ECommandsToActionType.MoveLeft2);

    }

    public void OnCommandSelected(ECommandsToActionType actionType)
    {
        if (currentPlayer == null)
            return;

        Vector3 targetMovePoint = Vector3.zero;

        switch (actionType)
        {
            case ECommandsToActionType.MoveLeft1:
                targetMovePoint = currentPlayer.transform.position + currentPlayer.transform.right * -3;
                currentPlayer.SetTargetMovePosition(targetMovePoint);
                break;

            case ECommandsToActionType.MoveLeft2:
                targetMovePoint = currentPlayer.transform.position + currentPlayer.transform.right * -6;
                currentPlayer.SetTargetMovePosition(targetMovePoint);
                break;

            case ECommandsToActionType.MoveForward1:
                targetMovePoint = currentPlayer.transform.position + currentPlayer.transform.forward * 4;
                currentPlayer.SetTargetMovePosition(targetMovePoint);
                break;

            case ECommandsToActionType.MoveForward2:
                targetMovePoint = currentPlayer.transform.position + currentPlayer.transform.forward * 8;
                currentPlayer.SetTargetMovePosition(targetMovePoint);
                break;

            case ECommandsToActionType.MoveRight1:
                targetMovePoint = currentPlayer.transform.position + currentPlayer.transform.right * 3;
                currentPlayer.SetTargetMovePosition(targetMovePoint);
                break;

            case ECommandsToActionType.MoveRight2:
                targetMovePoint = currentPlayer.transform.position + currentPlayer.transform.right * 6;
                currentPlayer.SetTargetMovePosition(targetMovePoint);
                break;

            case ECommandsToActionType.MoveForwardAndLeft1:
                break;
            case ECommandsToActionType.MoveForwardAndLeft2:
                break;
            case ECommandsToActionType.MoveForwardAndRight1:
                break;
            case ECommandsToActionType.MoveForwardAndRight2:
                break;
            case ECommandsToActionType.MoveBack:
                targetMovePoint = currentPlayer.transform.position + currentPlayer.transform.forward * -4;
                currentPlayer.SetTargetMovePosition(targetMovePoint);

                break;
            case ECommandsToActionType.Stop:
                break;
            case ECommandsToActionType.ShotOnGoal:
                break;
            case ECommandsToActionType.MakePass:
                break;
            case ECommandsToActionType.InterceptBall:
                break;
            case ECommandsToActionType.MarkOn:
                break;
            default:
                break;
        }
    }
}
