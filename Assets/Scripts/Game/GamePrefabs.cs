﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePrefabs : MonoBehaviour {

    private static GamePrefabs instance;

    public static GamePrefabs Instance
    {
        get { return instance; }
    }

    [SerializeField]
    public GameObject playerOnMapIndicatorPrefab;

    [SerializeField]
    public Material MoveArrowMaterial;
    [SerializeField]
    public Material DefenceArrowMaterial;
    [SerializeField]
    public Material PassArrowMaterial;
    [SerializeField]
    public Material ShotOnGoalArrowMaterial;

    [SerializeField]
    public Color MoveArrowColor;
    [SerializeField]
    public Color DefenceArrowColor;
    [SerializeField]
    public Color PassArrowColor;
    [SerializeField]
    public Color ShotOnGoalArrowColor;

    // Use this for initialization
    void Start () {
        instance = this;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public static Color GetColorByActionType(EPlayerActionType actionType)
    {
        switch (actionType)
        {
            case EPlayerActionType.Move:
                return GamePrefabs.Instance.MoveArrowColor;
                break;
            case EPlayerActionType.Defence:
                return GamePrefabs.Instance.DefenceArrowColor;
                break;
            case EPlayerActionType.Pass:
                return GamePrefabs.Instance.PassArrowColor;
                break;
            case EPlayerActionType.ShotOnGoal:
                return GamePrefabs.Instance.ShotOnGoalArrowColor;
                break;
            default:
                return GamePrefabs.Instance.MoveArrowColor;
                break;
        }
    }

    public static Color GetColorByAction(EPlayerAction action)
    {
        EPlayerActionType actionType = PlayerAction.GetActionTypeByAction(action);
        switch (actionType)
        {
            case EPlayerActionType.Move:
                return GamePrefabs.Instance.MoveArrowColor;
                break;
            case EPlayerActionType.Defence:
                return GamePrefabs.Instance.DefenceArrowColor;
                break;
            case EPlayerActionType.Pass:
                return GamePrefabs.Instance.PassArrowColor;
                break;
            case EPlayerActionType.ShotOnGoal:
                return GamePrefabs.Instance.ShotOnGoalArrowColor;
                break;
            default:
                return GamePrefabs.Instance.MoveArrowColor;
                break;
        }
    }
    public static Material GetMaterialByActionType(EPlayerActionType actionType)
    {
        switch (actionType)
        {
            case EPlayerActionType.Move:
                return GamePrefabs.Instance.MoveArrowMaterial;
                break;
            case EPlayerActionType.Defence:
                return GamePrefabs.Instance.DefenceArrowMaterial;
                break;
            case EPlayerActionType.Pass:
                return GamePrefabs.Instance.PassArrowMaterial;
                break;
            case EPlayerActionType.ShotOnGoal:
                return GamePrefabs.Instance.ShotOnGoalArrowMaterial;
                break;
            default:
                return GamePrefabs.Instance.MoveArrowMaterial;
                break;
        }
    }

    public static Material GetMaterialByAction(EPlayerAction action)
    {
        EPlayerActionType actionType = PlayerAction.GetActionTypeByAction(action);
        switch (actionType)
        {
            case EPlayerActionType.Move:
                return GamePrefabs.Instance.MoveArrowMaterial;
                break;
            case EPlayerActionType.Defence:
                return GamePrefabs.Instance.DefenceArrowMaterial;
                break;
            case EPlayerActionType.Pass:
                return GamePrefabs.Instance.PassArrowMaterial;
                break;
            case EPlayerActionType.ShotOnGoal:
                return GamePrefabs.Instance.ShotOnGoalArrowMaterial;
                break;
            default:
                return GamePrefabs.Instance.MoveArrowMaterial;
                break;
        }
    }
}
