﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMath : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static Vector2 GetPositionOnMap(Vector3 positionOnField)
    {
        Vector2 result = Vector2.zero;

        float fieldWidth = Mathf.Abs(FootballFieldController.FieldBorder1.position.x) + Mathf.Abs(FootballFieldController.FieldBorder2.position.x);
        float fieldHeight = Mathf.Abs(FootballFieldController.FieldBorder1.position.z) + Mathf.Abs(FootballFieldController.FieldBorder2.position.z);
        float mapWidth = TacticalCommandsPanelController.MapWidth;
        float mapHeight = TacticalCommandsPanelController.MapHeight;

        Vector2 mapPos = TacticalCommandsPanelController.MapCenter;
        float startX = mapPos.x;
        float startY = mapPos.y;

        float resultPosX = positionOnField.x / fieldWidth * mapWidth + startX;
        float resultPosY = positionOnField.z / fieldHeight * mapHeight + startY;

        return new Vector2(resultPosX, resultPosY);
    }

    public static Vector3 GetPositionOnField(Vector2 positionOnMap)
    {
        Vector3 result = Vector3.zero;

        return result;
    }

    public static bool IsPointInZone(Vector3 point, Vector3 zoneBorder1, Vector3 zoneBorder2)
    {
        bool result = false;

        if (IsValueInRange(point.x, zoneBorder1.x, zoneBorder2.x)
            && IsValueInRange(point.z, zoneBorder1.z, zoneBorder2.z))
            return true;

        return false;
    }

    public static bool IsValueInRange(float value, float border1, float border2)
    {
        float min = Mathf.Min(border1, border2);
        float max = Mathf.Max(border1, border2);

        if (min <= value && value <= max)
            return true;
        else
            return false;
    }
}
