﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum EGameState
{
    CommandPhase,
    PlayPhase,
    Pause
}
public class GameManager : MonoBehaviour {

    public static event System.Action OnCommandPhaseStarted;
    public static event System.Action OnPlayPhaseStarted;
    //public static event System.Action OnGamePhaseChanged;

    /// 
    /// Teams

    [SerializeField]
    private TeamController team1;

    [SerializeField]
    private TeamController team2;

    [SerializeField]
    private TeamController currentCoatchTeam;

    public TeamController CurrentCoatchTeam
    {
        get { return currentCoatchTeam; }
    }

    public TeamController Team1
    {
        get { return team1; }
    }
    public TeamController Team2
    {
        get { return team2; }
    }

    /// Game prefabs
    [SerializeField]
    GameObject PlayerActionsPanel;

    [SerializeField]
    GameObject TurnInfoPanel;

    [SerializeField]
    BallController ball;

    public BallController Ball
    {
        get { return ball; }
    }

    EGameState gameState;

    private static GameManager instance;
    public static GameManager Instance { get { return instance; } set { } }


    PlayerController currentSelectedPlayer;

    //[SerializeField]
    //GameObject SphereMarkerPrefab;

    void Start () {
        gameState = EGameState.CommandPhase;
        PlayerActionsPanel.SetActive(false);
        //TurnInfoPanel.SetActive(false);
        TurnInfoPanel.SetActive(true);
        PlayerController.OnMouseSelected += OnPlayerSelected;
        FootballFieldController.OnMouseSelected += OnPlayerSelected;
        
        instance = this;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space) && gameState != EGameState.Pause)
        {
            EGameState newState = gameState == EGameState.PlayPhase ? EGameState.CommandPhase : EGameState.PlayPhase;
            ChangeState(newState);
        }


        if (Input.GetMouseButtonDown(0))
        {
            //Ball.Move(Ball.transform.position + Ball.transform.right * 20);
        }

        //if(Input.GetMouseButtonDown(1))
        //{
        //    Vector3 p = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane - 20));
        //    SphereMarkerPrefab.transform.position = p;
        //}
    }

    void ChangeState(EGameState newState)
    {
        print("Change game state form " + gameState + " to " + newState);
        switch (newState)
        {
            case EGameState.CommandPhase:
                gameState = newState;
                StartCommandPhase();
                //OnGamePhaseChanged();
                break;

            case EGameState.PlayPhase:
                gameState = newState;
                StartPlayPhase();
                //OnGamePhaseChanged();
                break;

            case EGameState.Pause:
                gameState = newState;
                //OnGamePhaseChanged();
                break;
            default:
                break;
        }
    }

    public void Pause()
    {

    }

    public void StartCommandPhase()
    {
        if (OnCommandPhaseStarted != null)
            OnCommandPhaseStarted();

        //TurnInfoPanel.SetActive(true);

        gameState = EGameState.CommandPhase;

        currentCoatchTeam.NearestToBallPlayer.OnPlayerSelected();
    }

    public void EndPhase()
    {
        if (gameState == EGameState.CommandPhase)
            ChangeState(EGameState.PlayPhase);
        else if (gameState == EGameState.PlayPhase)
            ChangeState(EGameState.CommandPhase);

    }

    public void StartPlayPhase()
    {
        if (currentSelectedPlayer != null)
            currentSelectedPlayer.Unselect();
        currentSelectedPlayer = null;

        PlayerActionsPanel.SetActive(false);
        if (OnPlayPhaseStarted != null)
            OnPlayPhaseStarted();

        gameState = EGameState.PlayPhase;

    }

    void OnPlayerSelected(PlayerController player)
    {

        if (gameState != EGameState.CommandPhase)
            return;

        

        if (currentSelectedPlayer != null)
            currentSelectedPlayer.Unselect();

        if(player != null)
        {
            Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, player.transform.position.z);
            PlayerActionsPanel.SetActive(true);
            PlayerActionsPanel.GetComponent<TacticalCommandsPanelController>().CurrentPlayer = player;
            
            currentSelectedPlayer = player;
            currentSelectedPlayer.Select();
        }
    }

    public Vector3 GetPositionInFieldArea(FieldAreaInfo area)
    {
        Vector3 result = Vector3.zero;

        //return new Vector3(5, 0, 5);

        int areaInLine = 3;
        int areaInColumn = 7;

        int currLine = area.AreaID / areaInLine;
        int currPosInLine = area.AreaID - currLine * areaInLine;

        Transform FieldBorder1 = FootballFieldController.FieldBorder1;
        Transform FieldBorder2 = FootballFieldController.FieldBorder2;

        float pointX;

        float fieldWidth = Mathf.Abs(FieldBorder1.position.x) + Mathf.Abs(FieldBorder2.position.x);
        float fieldHeight = Mathf.Abs(FieldBorder1.position.z) + Mathf.Abs(FieldBorder2.position.z);

        float heightStep = fieldHeight / areaInColumn;
        float minZ = Mathf.Min(FieldBorder1.position.z, FieldBorder2.position.z);
        float pointZ1 = minZ + heightStep * currLine;
        float pointZ2 = pointZ1 + heightStep; //minZ + heightStep * currLine;
        //float Z = UnityEngine.Random.Range(pointZ1, pointZ2);
        float Z = (pointZ1 + pointZ2) / 2;

        float X;
        float X1 = 32.5f;
        float X2 = 20.0f;
        if (currPosInLine == 0)
        {
            //X = UnityEngine.Random.Range(-X1, -X2);
            X = (-X1 + -X2) / 2;
        }
        else if (currPosInLine == 1)
        {
            //X = UnityEngine.Random.Range(-X2, X2);
            X = (X2 + -X2) / 2;
        }
        else 
        {
            //X = UnityEngine.Random.Range(X1, X2);
            X = (X1 + X2) / 2;
        }

        result = new Vector3(X, 0, Z);

        return result;
    }

    public Vector3 GetBorderPositionInFieldArea(FieldAreaInfo area, bool leftDownBorder = false)
    {
        Vector3 result = Vector3.zero;

        //return new Vector3(5, 0, 5);

        int areaInLine = 3;
        int areaInColumn = 7;

        int currLine = area.AreaID / areaInLine;
        int currPosInLine = area.AreaID - currLine * areaInLine;

        Transform FieldBorder1 = FootballFieldController.FieldBorder1;
        Transform FieldBorder2 = FootballFieldController.FieldBorder2;

        float pointX;

        float fieldWidth = Mathf.Abs(FieldBorder1.position.x) + Mathf.Abs(FieldBorder2.position.x);
        float fieldHeight = Mathf.Abs(FieldBorder1.position.z) + Mathf.Abs(FieldBorder2.position.z);

        float heightStep = fieldHeight / areaInColumn;
        float minZ = Mathf.Min(FieldBorder1.position.z, FieldBorder2.position.z);
        float pointZ1 = minZ + heightStep * currLine;
        float pointZ2 = pointZ1 + heightStep;
        //minZ + heightStep * currLine;
        //float Z = UnityEngine.Random.Range(pointZ1, pointZ2);
        //float Z = (pointZ1 + pointZ2) / 2;
        float Z = leftDownBorder? pointZ1: pointZ2;

        float X;
        float X1 = 32.5f;
        float X2 = 20.0f;
        if (currPosInLine == 0)
        {
            //X = UnityEngine.Random.Range(-X1, -X2);
            //X = (-X1 + -X2) / 2;
            X = leftDownBorder ? -X1 : -X2;
        }
        else if (currPosInLine == 1)
        {
            //X = UnityEngine.Random.Range(-X2, X2);
            //X = (X2 + -X2) / 2;
            X = leftDownBorder ? -X2 : X2;
        }
        else
        {
            //X = UnityEngine.Random.Range(X1, X2);
            //X = (X1 + X2) / 2;
            X = leftDownBorder ? X1 : X2;
        }

        result = new Vector3(X, 0, Z);

        return result;
    }

    public TeamController GetEnemyTeam(TeamController ourTeam)
    {
        if (ourTeam == team1)
            return Team2;
        else
            return Team1;
    }

    public Vector3 GetTeamGatesPos(TeamController team)
    {
        if (team == team1)
            return FootballFieldController.GoalsPosition1.position;
        else
            return FootballFieldController.GoalsPosition2.position;
    }
 
}
